!(function (t) {
    function e(e) {
        for (var o, s, a = e[0], u = e[1], c = e[2], f = 0, p = []; f < a.length; f++) (s = a[f]), Object.prototype.hasOwnProperty.call(r, s) && r[s] && p.push(r[s][0]), (r[s] = 0);
        for (o in u) Object.prototype.hasOwnProperty.call(u, o) && (t[o] = u[o]);
        for (l && l(e); p.length; ) p.shift()();
        return i.push.apply(i, c || []), n();
    }
    function n() {
        for (var t, e = 0; e < i.length; e++) {
            for (var n = i[e], o = !0, a = 1; a < n.length; a++) {
                var u = n[a];
                0 !== r[u] && (o = !1);
            }
            o && (i.splice(e--, 1), (t = s((s.s = n[0]))));
        }
        return t;
    }
    var o = {},
        r = { 0: 0 },
        i = [];
    function s(e) {
        if (o[e]) return o[e].exports;
        var n = (o[e] = { i: e, l: !1, exports: {} });
        return t[e].call(n.exports, n, n.exports, s), (n.l = !0), n.exports;
    }
    (s.m = t),
        (s.c = o),
        (s.d = function (t, e, n) {
            s.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
        }),
        (s.r = function (t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
        }),
        (s.t = function (t, e) {
            if ((1 & e && (t = s(t)), 8 & e)) return t;
            if (4 & e && "object" == typeof t && t && t.__esModule) return t;
            var n = Object.create(null);
            if ((s.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t))
                for (var o in t)
                    s.d(
                        n,
                        o,
                        function (e) {
                            return t[e];
                        }.bind(null, o)
                    );
            return n;
        }),
        (s.n = function (t) {
            var e =
                t && t.__esModule
                    ? function () {
                          return t.default;
                      }
                    : function () {
                          return t;
                      };
            return s.d(e, "a", e), e;
        }),
        (s.o = function (t, e) {
            return Object.prototype.hasOwnProperty.call(t, e);
        }),
        (s.p = "");
    var a = (window.webpackJsonp = window.webpackJsonp || []),
        u = a.push.bind(a);
    (a.push = e), (a = a.slice());
    for (var c = 0; c < a.length; c++) e(a[c]);
    var l = u;
    i.push([31, 1]), n();
})({
    12: function (t, e, n) {},
    31: function (t, e, n) {
        "use strict";
        n.r(e);
        n(12);
        var o = n(0),
            r = n.n(o);
        var i = function () {
                var t = document.createElement("div");
                (t.style.overflowY = "scroll"), (t.style.width = "50px"), (t.style.height = "50px"), (t.style.opacity = "0"), (t.style.pointerEvents = "none"), document.body.appendChild(t);
                var e = t.offsetWidth - t.clientWidth;
                return t.remove(), e;
            },
            s = { stopScrollClassName: "stop-scroll", scrolling: !0, bodyWidth: 0, scroll: 0, $html: r()("html"), $body: r()("body") },
            a =
                (i(),
                function (t) {
                    if (
                        (void 0 === t && (t = !s.scrolling),
                        t !== s.scrolling &&
                            ((s.scrolling = t), (s.bodyWidth = s.$body.outerWidth()), t && (s.$body.css({ "padding-right": 0 }), s.$html.css({ top: 0 }), s.$html.removeClass(s.stopScrollClassName), window.scrollTo(0, s.scroll), c(!0)), !t))
                    ) {
                        var e = window.pageYOffset;
                        (s.scroll = e), s.$html.addClass(s.stopScrollClassName), s.$html.css({ top: -e }), s.$body.css({ "padding-right": s.$body.outerWidth() - s.bodyWidth }), c(!1);
                    }
                }),
            u = function () {
                for (
                    var t = [
                            { name: "mobile", media: "only screen and (max-width: 767px)" },
                            { name: "tablet", media: "only screen and (min-width: 768px) and (max-width: 1023px)" },
                            { name: "desktop", media: "only screen and (min-width: 1024px)" },
                        ],
                        e = 0;
                    e < t.length;
                    e++
                )
                    if (window.matchMedia(t[e].media).matches) return t[e].name;
            };
        var c = function (t) {
            void 0 === t && (t = !s.$body.hasClass("noChatsPlease")), s.$body.toggleClass("noChatsPlease", !t);
        };
        function l(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function f(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                (o.enumerable = o.enumerable || !1), (o.configurable = !0), "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
            }
        }
        var p = (function () {
            function t(e) {
                var n = this,
                    o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                l(this, t),
                    (this.timer = []),
                    (this.firstble = !0),
                    (this.windowLoaded = !1),
                    (this.settings = r.a.extend({ debug: !1 }, o)),
                    (this.$window = r()(window)),
                    (this.$body = r()("body")),
                    (this.$component = r()(e)),
                    this.$window.on("load", function () {
                        n.windowLoaded = !0;
                    });
            }
            var e, n, o;
            return (
                (e = t),
                (n = [
                    {
                        key: "componentName",
                        value: function () {
                            return "Component className is not defined";
                        },
                    },
                    {
                        key: "namespace",
                        value: function () {
                            return "." + this.componentName();
                        },
                    },
                    {
                        key: "writeLog",
                        value: function (t) {
                            this.settings.debug && (this.firstble && (console.clear(), console.log("debug mode"), console.log("Component: " + this.componentName()), (this.firstble = !1)), console.log(t));
                        },
                    },
                    {
                        key: "unrepeatableDelay",
                        value: function (t, e) {
                            var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 0;
                            this.clearUnrepeatableDelay(n),
                                (this.timer[n] = setTimeout(function () {
                                    t();
                                }, e));
                        },
                    },
                    {
                        key: "clearUnrepeatableDelay",
                        value: function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                            void 0 !== this.timer[t] && clearTimeout(this.timer[t]);
                        },
                    },
                    { key: "init", value: function () {} },
                    { key: "destroy", value: function () {} },
                ]) && f(e.prototype, n),
                o && f(e, o),
                t
            );
        })();
        function h(t) {
            return (h =
                "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
                    ? function (t) {
                          return typeof t;
                      }
                    : function (t) {
                          return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
                      })(t);
        }
        function y(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function m(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                (o.enumerable = o.enumerable || !1), (o.configurable = !0), "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
            }
        }
        function g(t, e) {
            return (g =
                Object.setPrototypeOf ||
                function (t, e) {
                    return (t.__proto__ = e), t;
                })(t, e);
        }
        function d(t) {
            var e = (function () {
                if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
                if (Reflect.construct.sham) return !1;
                if ("function" == typeof Proxy) return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
                } catch (t) {
                    return !1;
                }
            })();
            return function () {
                var n,
                    o = v(t);
                if (e) {
                    var r = v(this).constructor;
                    n = Reflect.construct(o, arguments, r);
                } else n = o.apply(this, arguments);
                return b(this, n);
            };
        }
        function b(t, e) {
            return !e || ("object" !== h(e) && "function" != typeof e)
                ? (function (t) {
                      if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                      return t;
                  })(t)
                : e;
        }
        function v(t) {
            return (v = Object.setPrototypeOf
                ? Object.getPrototypeOf
                : function (t) {
                      return t.__proto__ || Object.getPrototypeOf(t);
                  })(t);
        }
        var w = (function (t) {
            !(function (t, e) {
                if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
                (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })), e && g(t, e);
            })(s, t);
            var e,
                n,
                o,
                i = d(s);
            function s(t) {
                var e,
                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                return (
                    y(this, s), ((e = i.call(this, t, r.a.extend({ debug: !1, smallClassName: ".header_small", target: ".screen1__text", border: { mobile: 77, tablet: 95, desktop: 95 } }, n))).$target = r()(e.settings.target)), e.init(), e
                );
            }
            return (
                (e = s),
                (n = [
                    {
                        key: "componentName",
                        value: function () {
                            return "Header";
                        },
                    },
                    {
                        key: "toggleHeaderClass",
                        value: function () {
                            var t = this.$target[0].getBoundingClientRect().top,
                                e = 0;
                            "mobile" === u() && (e = this.settings.border.mobile),
                                "tablet" === u() && (e = this.settings.border.tablet),
                                "desktop" === u() && (e = this.settings.border.desktop),
                                this.$component.toggleClass(this.settings.smallClassName.slice(1), t < e);
                        },
                    },
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            this.writeLog("---"),
                                this.writeLog("Method init:"),
                                this.writeLog("---"),
                                this.writeLog("$component:"),
                                this.writeLog(this.$component),
                                this.writeLog("---"),
                                this.writeLog("settings:"),
                                this.writeLog(this.settings),
                                this.writeLog("---"),
                                this.writeLog("target:"),
                                this.writeLog(this.$target),
                                this.toggleHeaderClass(),
                                this.$window.on("scroll", function () {
                                    t.toggleHeaderClass();
                                }),
                                this.$window.on("resize", function () {
                                    t.unrepeatableDelay(
                                        function () {
                                            t.toggleHeaderClass();
                                        },
                                        300,
                                        2
                                    );
                                });
                        },
                    },
                ]) && m(e.prototype, n),
                o && m(e, o),
                s
            );
        })(p);
        function O(t) {
            return (O =
                "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
                    ? function (t) {
                          return typeof t;
                      }
                    : function (t) {
                          return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
                      })(t);
        }
        function k(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function $(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                (o.enumerable = o.enumerable || !1), (o.configurable = !0), "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
            }
        }
        function _(t, e) {
            return (_ =
                Object.setPrototypeOf ||
                function (t, e) {
                    return (t.__proto__ = e), t;
                })(t, e);
        }
        function L(t) {
            var e = (function () {
                if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
                if (Reflect.construct.sham) return !1;
                if ("function" == typeof Proxy) return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
                } catch (t) {
                    return !1;
                }
            })();
            return function () {
                var n,
                    o = j(t);
                if (e) {
                    var r = j(this).constructor;
                    n = Reflect.construct(o, arguments, r);
                } else n = o.apply(this, arguments);
                return P(this, n);
            };
        }
        function P(t, e) {
            return !e || ("object" !== O(e) && "function" != typeof e)
                ? (function (t) {
                      if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                      return t;
                  })(t)
                : e;
        }
        function j(t) {
            return (j = Object.setPrototypeOf
                ? Object.getPrototypeOf
                : function (t) {
                      return t.__proto__ || Object.getPrototypeOf(t);
                  })(t);
        }
        var C = (function (t) {
            !(function (t, e) {
                if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
                (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })), e && _(t, e);
            })(s, t);
            var e,
                n,
                o,
                i = L(s);
            function s(t) {
                var e,
                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                return k(this, s), (e = i.call(this, t, r.a.extend({ debug: !1, button: ".js-toggle-touch-menu", showClassName: "touch-menu_show" }, n))).init(), e;
            }
            return (
                (e = s),
                (n = [
                    {
                        key: "componentName",
                        value: function () {
                            return "TouchMenu";
                        },
                    },
                    {
                        key: "state",
                        value: function () {
                            return this.$component.hasClass(this.settings.showClassName);
                        },
                    },
                    {
                        key: "toggle",
                        value: function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : !this.state();
                            this.$component.toggleClass(this.settings.showClassName, t), a(!t);
                        },
                    },
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            this.writeLog("---"),
                                this.writeLog("Method init:"),
                                this.writeLog("---"),
                                this.writeLog("$component:"),
                                this.writeLog(this.$component),
                                this.writeLog("---"),
                                this.writeLog("settings:"),
                                this.writeLog(this.settings),
                                this.$body.on("click", this.settings.button, function () {
                                    t.toggle();
                                });
                        },
                    },
                ]) && $(e.prototype, n),
                o && $(e, o),
                s
            );
        })(p);
        function S(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function N(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                (o.enumerable = o.enumerable || !1), (o.configurable = !0), "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
            }
        }
        var R = (function () {
            function t() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                S(this, t), (this.timer = []), (this.firstble = !0), (this.settings = r.a.extend({ debug: !1 }, e)), (this.$html = r()("html")), (this.$window = r()(window)), (this.$body = r()("body"));
            }
            var e, n, o;
            return (
                (e = t),
                (n = [
                    {
                        key: "componentName",
                        value: function () {
                            return "Component className is not defined";
                        },
                    },
                    {
                        key: "namespace",
                        value: function () {
                            return "." + this.componentName();
                        },
                    },
                    {
                        key: "writeLog",
                        value: function (t) {
                            this.settings.debug && (this.firstble && (console.clear(), console.log("debug mode"), console.log("Component: " + this.componentName()), (this.firstble = !1)), console.log(t));
                        },
                    },
                    {
                        key: "unrepeatableDelay",
                        value: function (t, e) {
                            var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 0;
                            this.clearUnrepeatableDelay(n),
                                (this.timer[n] = setTimeout(function () {
                                    t();
                                }, e));
                        },
                    },
                    {
                        key: "clearUnrepeatableDelay",
                        value: function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                            void 0 !== this.timer[t] && clearTimeout(this.timer[t]);
                        },
                    },
                    { key: "init", value: function () {} },
                    { key: "destroy", value: function () {} },
                ]) && N(e.prototype, n),
                o && N(e, o),
                t
            );
        })();
        function x(t) {
            return (x =
                "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
                    ? function (t) {
                          return typeof t;
                      }
                    : function (t) {
                          return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
                      })(t);
        }
        function E(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function T(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                (o.enumerable = o.enumerable || !1), (o.configurable = !0), "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
            }
        }
        function B(t, e) {
            return (B =
                Object.setPrototypeOf ||
                function (t, e) {
                    return (t.__proto__ = e), t;
                })(t, e);
        }
        function D(t) {
            var e = (function () {
                if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
                if (Reflect.construct.sham) return !1;
                if ("function" == typeof Proxy) return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
                } catch (t) {
                    return !1;
                }
            })();
            return function () {
                var n,
                    o = I(t);
                if (e) {
                    var r = I(this).constructor;
                    n = Reflect.construct(o, arguments, r);
                } else n = o.apply(this, arguments);
                return M(this, n);
            };
        }
        function M(t, e) {
            return !e || ("object" !== x(e) && "function" != typeof e)
                ? (function (t) {
                      if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                      return t;
                  })(t)
                : e;
        }
        function I(t) {
            return (I = Object.setPrototypeOf
                ? Object.getPrototypeOf
                : function (t) {
                      return t.__proto__ || Object.getPrototypeOf(t);
                  })(t);
        }
        var F = (function (t) {
            !(function (t, e) {
                if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
                (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })), e && B(t, e);
            })(s, t);
            var e,
                n,
                o,
                i = D(s);
            function s(t) {
                var e,
                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                return (
                    E(this, s),
                    (e = i.call(
                        this,
                        r.a.extend(
                            { debug: !1, self: ".ui-input:not(.ui-input-vue)", errorClassName: ".ui-input_error", errorDiv: ".ui-input__error", switchType: { button: ".js-ui-input-toggle-type", showClassName: ".ui-input__switch_show" } },
                            n
                        )
                    )).init(),
                    e
                );
            }
            return (
                (e = s),
                (n = [
                    {
                        key: "componentName",
                        value: function () {
                            return "Input";
                        },
                    },
                    {
                        key: "namespace",
                        value: function () {
                            return "." + this.componentName();
                        },
                    },
                    {
                        key: "init",
                        value: function () {
                            this.writeLog("---"), this.writeLog("Method init"), this.writeLog("---"), this.writeLog("settings:"), this.writeLog(this.settings);
                            var t = this;
                            this.$body.on("click" + this.namespace(), this.settings.switchType.button, function (e) {
                                e.preventDefault();
                                var n = r()(this),
                                    o = n.siblings("input"),
                                    i = o.attr("type");
                                t.writeLog("---"),
                                    t.writeLog('Event "click" by toggle button ('.concat(t.settings.switchType.button, ")")),
                                    t.writeLog("input:"),
                                    t.writeLog(o),
                                    t.writeLog("type: " + i),
                                    n.toggleClass(t.settings.switchType.showClassName.slice(1), "password" === i),
                                    o.attr("type", "password" === i ? "text" : "password");
                            }),
                                this.$body.on("click keydown", this.settings.self, function () {
                                    var e = r()(this),
                                        n = e.find(t.settings.errorDiv.slice(1));
                                    e.removeClass(t.settings.errorClassName.slice(1)), n.text("");
                                });
                        },
                    },
                ]) && T(e.prototype, n),
                o && T(e, o),
                s
            );
        })(R);
        function H(t) {
            return (H =
                "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
                    ? function (t) {
                          return typeof t;
                      }
                    : function (t) {
                          return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
                      })(t);
        }
        function W(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function q(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                (o.enumerable = o.enumerable || !1), (o.configurable = !0), "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
            }
        }
        function z(t, e) {
            return (z =
                Object.setPrototypeOf ||
                function (t, e) {
                    return (t.__proto__ = e), t;
                })(t, e);
        }
        function U(t) {
            var e = (function () {
                if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
                if (Reflect.construct.sham) return !1;
                if ("function" == typeof Proxy) return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
                } catch (t) {
                    return !1;
                }
            })();
            return function () {
                var n,
                    o = Y(t);
                if (e) {
                    var r = Y(this).constructor;
                    n = Reflect.construct(o, arguments, r);
                } else n = o.apply(this, arguments);
                return J(this, n);
            };
        }
        function J(t, e) {
            return !e || ("object" !== H(e) && "function" != typeof e)
                ? (function (t) {
                      if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                      return t;
                  })(t)
                : e;
        }
        function Y(t) {
            return (Y = Object.setPrototypeOf
                ? Object.getPrototypeOf
                : function (t) {
                      return t.__proto__ || Object.getPrototypeOf(t);
                  })(t);
        }
        var Q = (function (t) {
            !(function (t, e) {
                if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
                (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })), e && z(t, e);
            })(s, t);
            var e,
                n,
                o,
                i = U(s);
            function s(t) {
                var e,
                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                return W(this, s), (e = i.call(this, r.a.extend({ debug: !1, self: "input[type=tel]", startFrom: "+7 (", mask: "+7 (000) 000-00-00", literalPattern: /[0\*]/, numberPattern: /[0-9]/ }, n))).init(), e;
            }
            return (
                (e = s),
                (n = [
                    {
                        key: "componentName",
                        value: function () {
                            return "InputPhone";
                        },
                    },
                    {
                        key: "namespace",
                        value: function () {
                            return "." + this.componentName();
                        },
                    },
                    {
                        key: "maskInput",
                        value: function (t) {
                            var e = "";
                            try {
                                for (var n = this.settings.mask.length, o = 0, r = 0; r < n && !(r >= t.value.length) && ("0" !== this.settings.mask[r] || null !== t.value[o].match(this.settings.numberPattern)); ) {
                                    for (; null === this.settings.mask[r].match(this.settings.literalPattern) && t.value[o] !== this.settings.mask[r]; ) e += this.settings.mask[r++];
                                    (e += t.value[o++]), r++;
                                }
                                t.value = e;
                            } catch (t) {
                                console.log(t);
                            }
                        },
                    },
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            this.$body.on("input" + this.namespace(), this.settings.self, function (e) {
                                t.maskInput(this);
                            }),
                                this.$body.on("focus", this.settings.self, function () {
                                    this.value.length < 1 && (this.value = t.settings.startFrom);
                                }),
                                this.$body.on("blur", this.settings.self, function () {
                                    this.value === t.settings.startFrom && (this.value = "");
                                });
                        },
                    },
                ]) && q(e.prototype, n),
                o && q(e, o),
                s
            );
        })(R);
        function Z(t) {
            return (Z =
                "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
                    ? function (t) {
                          return typeof t;
                      }
                    : function (t) {
                          return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
                      })(t);
        }
        function A(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function G(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                (o.enumerable = o.enumerable || !1), (o.configurable = !0), "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
            }
        }
        function K(t, e) {
            return (K =
                Object.setPrototypeOf ||
                function (t, e) {
                    return (t.__proto__ = e), t;
                })(t, e);
        }
        function V(t) {
            var e = (function () {
                if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
                if (Reflect.construct.sham) return !1;
                if ("function" == typeof Proxy) return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
                } catch (t) {
                    return !1;
                }
            })();
            return function () {
                var n,
                    o = tt(t);
                if (e) {
                    var r = tt(this).constructor;
                    n = Reflect.construct(o, arguments, r);
                } else n = o.apply(this, arguments);
                return X(this, n);
            };
        }
        function X(t, e) {
            return !e || ("object" !== Z(e) && "function" != typeof e)
                ? (function (t) {
                      if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                      return t;
                  })(t)
                : e;
        }
        function tt(t) {
            return (tt = Object.setPrototypeOf
                ? Object.getPrototypeOf
                : function (t) {
                      return t.__proto__ || Object.getPrototypeOf(t);
                  })(t);
        }
        var et = (function (t) {
            !(function (t, e) {
                if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
                (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })), e && K(t, e);
            })(s, t);
            var e,
                n,
                o,
                i = V(s);
            function s(t) {
                var e,
                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                return A(this, s), (e = i.call(this, r.a.extend({ debug: !1, self: "input[data-type=date]", mask: "00.00.0000", literalPattern: /[0\*]/, numberPattern: /[0-9]/ }, n))).init(), e;
            }
            return (
                (e = s),
                (n = [
                    {
                        key: "componentName",
                        value: function () {
                            return "InputDate";
                        },
                    },
                    {
                        key: "namespace",
                        value: function () {
                            return "." + this.componentName();
                        },
                    },
                    {
                        key: "maskInput",
                        value: function (t) {
                            var e = "";
                            try {
                                for (var n = this.settings.mask.length, o = 0, r = 0; r < n && !(r >= t.value.length) && ("0" !== this.settings.mask[r] || null !== t.value[o].match(this.settings.numberPattern)); ) {
                                    for (; null === this.settings.mask[r].match(this.settings.literalPattern) && t.value[o] !== this.settings.mask[r]; ) e += this.settings.mask[r++];
                                    (e += t.value[o++]), r++;
                                }
                                t.value = e;
                            } catch (t) {
                                console.log(t);
                            }
                        },
                    },
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            this.$body.on("input" + this.namespace(), this.settings.self, function (e) {
                                t.maskInput(this);
                            });
                        },
                    },
                ]) && G(e.prototype, n),
                o && G(e, o),
                s
            );
        })(R);
        function nt(t) {
            return (nt =
                "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
                    ? function (t) {
                          return typeof t;
                      }
                    : function (t) {
                          return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
                      })(t);
        }
        function ot(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function rt(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                (o.enumerable = o.enumerable || !1), (o.configurable = !0), "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
            }
        }
        function it(t, e) {
            return (it =
                Object.setPrototypeOf ||
                function (t, e) {
                    return (t.__proto__ = e), t;
                })(t, e);
        }
        function st(t) {
            var e = (function () {
                if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
                if (Reflect.construct.sham) return !1;
                if ("function" == typeof Proxy) return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
                } catch (t) {
                    return !1;
                }
            })();
            return function () {
                var n,
                    o = ut(t);
                if (e) {
                    var r = ut(this).constructor;
                    n = Reflect.construct(o, arguments, r);
                } else n = o.apply(this, arguments);
                return at(this, n);
            };
        }
        function at(t, e) {
            return !e || ("object" !== nt(e) && "function" != typeof e)
                ? (function (t) {
                      if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                      return t;
                  })(t)
                : e;
        }
        function ut(t) {
            return (ut = Object.setPrototypeOf
                ? Object.getPrototypeOf
                : function (t) {
                      return t.__proto__ || Object.getPrototypeOf(t);
                  })(t);
        }
        var ct = (function (t) {
                !(function (t, e) {
                    if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
                    (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })), e && it(t, e);
                })(s, t);
                var e,
                    n,
                    o,
                    i = st(s);
                function s(t) {
                    var e,
                        n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                    return ot(this, s), (e = i.call(this, r.a.extend({ self: 'input[data-type="number"]' }, n))).init(), e;
                }
                return (
                    (e = s),
                    (n = [
                        {
                            key: "componentName",
                            value: function () {
                                return "InputNumber";
                            },
                        },
                        {
                            key: "namespace",
                            value: function () {
                                return "." + this.componentName();
                            },
                        },
                        {
                            key: "init",
                            value: function () {
                                this.$body.on("input" + this.namespace(), this.settings.self, function () {
                                    this.value = this.value.replace(/[^0-9]/gim, "");
                                });
                            },
                        },
                    ]) && rt(e.prototype, n),
                    o && rt(e, o),
                    s
                );
            })(R),
            lt = n(10),
            ft = n(11),
            pt = n.n(ft);
        function ht(t) {
            return (ht =
                "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
                    ? function (t) {
                          return typeof t;
                      }
                    : function (t) {
                          return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
                      })(t);
        }
        function yt(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function mt(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                (o.enumerable = o.enumerable || !1), (o.configurable = !0), "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
            }
        }
        function gt(t, e) {
            return (gt =
                Object.setPrototypeOf ||
                function (t, e) {
                    return (t.__proto__ = e), t;
                })(t, e);
        }
        function dt(t) {
            var e = (function () {
                if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
                if (Reflect.construct.sham) return !1;
                if ("function" == typeof Proxy) return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
                } catch (t) {
                    return !1;
                }
            })();
            return function () {
                var n,
                    o = vt(t);
                if (e) {
                    var r = vt(this).constructor;
                    n = Reflect.construct(o, arguments, r);
                } else n = o.apply(this, arguments);
                return bt(this, n);
            };
        }
        function bt(t, e) {
            return !e || ("object" !== ht(e) && "function" != typeof e)
                ? (function (t) {
                      if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                      return t;
                  })(t)
                : e;
        }
        function vt(t) {
            return (vt = Object.setPrototypeOf
                ? Object.getPrototypeOf
                : function (t) {
                      return t.__proto__ || Object.getPrototypeOf(t);
                  })(t);
        }
        var wt = (function (t) {
            !(function (t, e) {
                if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
                (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })), e && gt(t, e);
            })(s, t);
            var e,
                n,
                o,
                i = dt(s);
            function s(t) {
                var e,
                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                return (
                    yt(this, s),
                    ((e = i.call(
                        this,
                        t,
                        r.a.extend(
                            {
                                debug: !1,
                                url: "./send-form.php",
                                showClassName: "query-form_show",
                                wrapper: ".query-form__wrapper",
                                loadingClassName: "query-form__wrapper_loading",
                                hiddenClassName: "hidden",
                                form: ".form",
                                success: ".form__success",
                                name: "input[name=name]",
                                company: "input[name=company]",
                                phone: "input[name=phone]",
                                email: "input[name=email]",
                                message: "textarea[name=message]",
                                toggleForm: ".js-toggle-query-form",
                            },
                            n
                        )
                    )).$wrapper = e.$component.find(e.settings.wrapper)),
                    (e.$success = e.$component.find(e.settings.success)),
                    (e.$name = e.$component.find(e.settings.name)),
                    (e.$company = e.$component.find(e.settings.company)),
                    (e.$phone = e.$component.find(e.settings.phone)),
                    (e.$email = e.$component.find(e.settings.email)),
                    (e.$message = e.$component.find(e.settings.message)),
                    (e.$form = e.$component.find(e.settings.form)),
                    e.init(),
                    e
                );
            }
            return (
                (e = s),
                (n = [
                    {
                        key: "componentName",
                        value: function () {
                            return "QueryForm";
                        },
                    },
                    {
                        key: "validName",
                        value: function (t) {
                            return /^[a-zа-я-\s]+$/i.test(t);
                        },
                    },
                    {
                        key: "validPhone",
                        value: function (t) {
                            return /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10}$/i.test(t);
                        },
                    },
                    {
                        key: "setError",
                        value: function (t, e) {
                            this.writeLog("---"),
                                this.writeLog("Method setError:"),
                                this.writeLog("where:"),
                                this.writeLog(t),
                                this.writeLog("text:"),
                                this.writeLog(e),
                                t.parent().addClass("ui-input_error"),
                                t.parent().find(".ui-input__error").text(e);
                        },
                    },
                    {
                        key: "sendForm",
                        value: function () {
                            var t = this;
                            this.writeLog("---"), this.writeLog("sendForm"), this.$wrapper.addClass(this.settings.loadingClassName);
                            var e = new FormData(this.$form.get(0));
                            pt.a
                                .post(this.settings.url, e)
                                .then(function () {
                                    t.$wrapper.removeClass(t.settings.loadingClassName), t.$form.addClass(t.settings.hiddenClassName), t.$success.removeClass(t.settings.hiddenClassName);
                                })
                                .catch(function () {
                                    t.writeLog("---"), t.writeLog("post send error");
                                });
                        },
                    },
                    {
                        key: "state",
                        value: function () {
                            return this.$component.hasClass(this.settings.showClassName);
                        },
                    },
                    {
                        key: "toggle",
                        value: function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : !this.state();
                            this.$component.toggleClass(this.settings.showClassName, t), a(!t);
                        },
                    },
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            this.writeLog("---"),
                                this.writeLog("Method init"),
                                this.writeLog("---"),
                                this.writeLog("$component:"),
                                this.writeLog(this.$component),
                                this.writeLog("---"),
                                this.writeLog("settings:"),
                                this.writeLog(this.settings),
                                this.writeLog("---"),
                                this.writeLog("form:"),
                                this.writeLog(this.$form),
                                this.writeLog("---"),
                                this.writeLog("name:"),
                                this.writeLog(this.$name),
                                this.writeLog("---"),
                                this.writeLog("company:"),
                                this.writeLog(this.$company),
                                this.writeLog("---"),
                                this.writeLog("phone:"),
                                this.writeLog(this.$phone),
                                this.writeLog("---"),
                                this.writeLog("email:"),
                                this.writeLog(this.$email),
                                this.writeLog("---"),
                                this.writeLog("message:"),
                                this.writeLog(this.$message),
                                this.$body.on("click", this.settings.toggleForm, function () {
                                    t.toggle();
                                }),
                                this.$form.on("submit" + this.namespace(), function (e) {
                                    e.preventDefault(), t.writeLog("---"), t.writeLog("Event submit form");
                                    var n = t.$name.val(),
                                        o = t.$email.val(),
                                        r = t.$phone.val(),
                                        i = !1;
                                    (!n || n.length < 2) && (t.setError(t.$name, "Слишком короткое имя"), (i = !0)),
                                        !t.validName(n) && n.length >= 2 && (t.setError(t.$name, "Допускаются только буквы"), (i = !0)),
                                        (!r || r.length < 2) && (t.setError(t.$phone, "Укажите телефон"), (i = !0)),
                                        !t.validPhone(r) && r.length >= 2 && (t.setError(t.$phone, "Телефон указан с ошибкой"), (i = !0)),
                                        o.length > 0 && !lt.validate(o) && t.setError(t.$email, "E-mail указан с ошибкой"),
                                        i || t.sendForm();
                                });
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            this.writeLog("---"), this.writeLog("Method destroy"), this.$window.off(this.namespace()), this.$component.off(this.namespace());
                        },
                    },
                ]) && mt(e.prototype, n),
                o && mt(e, o),
                s
            );
        })(p);
        new w(".header", { debug: !1 });
        var Ot = new C(".touch-menu", { debug: !1 });
        new F(), new Q(), new et(), new ct(), new wt(".query-form", { debug: !1 });
        var kt = function (t) {
            var e = document.documentElement.clientHeight,
                n = t[0].offsetHeight,
                o = n >= e ? t[0].offsetTop - 90 : t[0].offsetTop - e / 2 + n / 2;
            window.scrollTo({ top: o, behavior: "smooth" });
        };
        r()(".js-scroll-to").on("click", function (t) {
            var e = r()("#".concat(t.target.dataset.ref));
            if (!e.length) return !1;
            "desktop" !== u()
                ? (Ot.toggle(!1),
                  setTimeout(function () {
                      kt(e);
                  }, 300))
                : kt(e);
        });
        var $t = { iconLayout: "default#imageWithContent", iconImageHref: "/front-end/assets/img/mapPickPoint.png", iconImageSize: [62, 69], iconImageOffset: [-31, -69] };
        ymaps.ready(function () {
            var t = new ymaps.Map("map", { center: [55.709312, 37.65452], zoom: 13 });
            t.behaviors.disable("scrollZoom"), t.geoObjects.add(new ymaps.Placemark([55.709312, 37.65452], {}, $t));
        });
        var _t = function () {
            document.querySelector("body").classList.remove("loading");
        };
        "loading" === document.readyState ? document.addEventListener("DOMContentLoaded", _t) : _t();
    },
});
